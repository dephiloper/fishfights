extends Area2D

export (int) var SPEED
export (int) var STRETCH_BACK_STRENGTH = 2
export (int) var LAUNCH_STRENGTH = 8
export (int) var THROW_SPEED = 200
export (int) var SINK_SPEED = 100

enum State {
	MOVEMENT,
	STRETCH_ROD,
	LAUNCH_ROD,
	THROW_SWIMMER,
	SINKING_SWIMMER
}

enum Area {
  	LEFT,
  	RIGHT
}

var currentArea = -1
var throwVelocity = Vector2()
var maxRotation = 1.4
var speedReduction = 0.04
var stretchedRotation = 0
var currentState = MOVEMENT
var resetPositionRod = Vector2()

func _ready():
	pass
	
func _physics_process(delta):
	match currentState:
		MOVEMENT:
			handle_movement(delta)
			throwVelocity = Vector2(-THROW_SPEED, -THROW_SPEED/2)
		STRETCH_ROD:
			handle_fishing_rod_stretch(delta)
		LAUNCH_ROD:
			handle_fishing_rod_launch(delta)
		THROW_SWIMMER:
			handle_launch_swimmer(delta)
		SINKING_SWIMMER:
			handle_sinking_swimmer(delta)

func handle_movement(delta):
	var velocity = Vector2()
	$FishingRodSprite.set_texture(load("res://fishing_rod_line.png"))
	if(Input.is_key_pressed(KEY_LEFT) && currentArea != Area.LEFT):
		velocity.x -= 1
	if(Input.is_key_pressed(KEY_RIGHT)  && currentArea != Area.RIGHT):
		velocity.x += 1
	if velocity.length() > 0:
        velocity = velocity.normalized() * SPEED
	position += velocity * delta
	
	if(Input.is_key_pressed(KEY_ENTER)):
		currentState = STRETCH_ROD
		resetPositionRod = $FishingRodSprite/FishingRodSwimmer/SwimmerSprite.global_position

func handle_fishing_rod_stretch(delta):
	if(Input.is_key_pressed(KEY_ENTER)):
		if ($FishingRodSprite.rotation < maxRotation):
			$FishingRodSprite.rotate(delta * STRETCH_BACK_STRENGTH)
	else:
		currentState = LAUNCH_ROD
		stretchedRotation = $FishingRodSprite.rotation

func handle_fishing_rod_launch(delta):
	if($FishingRodSprite.rotation > 0):
		$FishingRodSprite.set_texture(load("res://fishing_rod_no_line.png"))
		$FishingRodSprite.rotate(-delta * LAUNCH_STRENGTH)
	else:
		currentState = THROW_SWIMMER
		
func handle_launch_swimmer(delta):
	if (stretchedRotation < 0.5):
		stretchedRotation = 0.5
	$FishingRodSprite/FishingRodSwimmer.position += throwVelocity * delta * stretchedRotation
	throwVelocity.y += 2

func handle_sinking_swimmer(delta):
	$FishingRodSprite/FishingRodSwimmer.position += Vector2(0, SINK_SPEED) * delta

func _on_HumanPlayer_area_shape_entered(area_id, area, area_shape, self_shape):
	currentArea = area_shape

func _on_HumanPlayer_area_shape_exited(area_id, area, area_shape, self_shape):
	currentArea = -1