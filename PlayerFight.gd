extends KinematicBody2D

export (int) var RUN_SPEED = 300
export (int) var JUMP_SPEED = -600
export (int) var GRAVITY = 1000
export (int) var HEALTH = 40
export (bool) var IS_HUMAN = false

export (String) var RIGHT_ACTION
export (String) var LEFT_ACTION
export (String) var JUMP_ACTION
export (String) var HIT_ACTION

enum State {
	MOVEMENT,
	HIT
}

var velocity = Vector2()
var currentState = MOVEMENT
var frameCounter = 0
var jumping = false
var hitted = false

func _ready():
	if !IS_HUMAN:
		HEALTH += Global.lives
		if HEALTH > 60: HEALTH = 60

func get_input():
	velocity.x = 0
	var right = Input.is_action_pressed(RIGHT_ACTION)
	var left = Input.is_action_pressed(LEFT_ACTION)
	var jump = Input.is_action_just_pressed(JUMP_ACTION)
	var hit = Input.is_action_just_pressed(HIT_ACTION)

	if jump and is_on_floor():
		jumping = true
		velocity.y = JUMP_SPEED
	if right:
		velocity.x += RUN_SPEED
		$AnimatedSprite.flip_h = true
		$FistArea/FistRight.disabled = !IS_HUMAN
		$FistArea/FistLeft.disabled = IS_HUMAN
	if left:
		velocity.x -= RUN_SPEED
		$AnimatedSprite.flip_h = false
		$FistArea/FistRight.disabled = IS_HUMAN
		$FistArea/FistLeft.disabled = !IS_HUMAN
	if hit:
		currentState = HIT
		$AnimatedSprite.play("hit")

func _physics_process(delta):
	get_input()
	velocity.y += GRAVITY * delta
	if jumping and is_on_floor():
		jumping = false
	velocity = move_and_slide(velocity, Vector2(0, -1))
	
	if currentState == HIT:
		frameCounter += 1
	
	if frameCounter == 20:
		$AnimatedSprite.play("default")
		currentState = MOVEMENT
		frameCounter = 0
		hitted = false
	
	if HEALTH < 0:
		queue_free()
		$"/root/Fight/Label".show()
		if IS_HUMAN:
			$"/root/Fight/Label".set_text("FISH WINS")
		else:
			$"/root/Fight/Label".set_text("HUMAN WINS")
		$"/root/Fight/ResetTimer".start()
		
		
	
	if currentState == HIT:
		var body
		if IS_HUMAN:
			body = $"/root/Fight/FishPlayer"
		else:
			body = $"/root/Fight/HumanPlayer"
		
		if $FistArea.overlaps_body(body) && !hitted:
			if $AnimatedSprite.flip_h == true:
				body.move_and_collide(Vector2(60, -40))
			else:
				body.move_and_collide(Vector2(-60, -40))
			body.HEALTH -= 2
			hitted = true
			
	$Lives.play(str(HEALTH/10))

func _on_ResetTimer_timeout():
	Global.lives = 0
	get_tree().change_scene("res://Land.tscn")
