extends Area2D

export (int) var SPEED
export (int) var HEALTH = 0

var currentAreas = []

enum AREA{
  	TOP,
  	BOTTOM,
	LEFT,
	RIGHT
}

func _ready():
	$AnimatedSprite.play()

func _process(delta):
	handle_movement(delta)
	$AnimatedSprite.set_scale(Vector2(1 + 0.02 * HEALTH, 1 + 0.02 * HEALTH))
func handle_movement(delta):
	var velocity = Vector2()
	
	if(Input.is_key_pressed(KEY_W) && ! currentAreas.has(AREA.TOP)):
		velocity.y -= 1
	if(Input.is_key_pressed(KEY_A) && ! currentAreas.has(AREA.LEFT)):
		velocity.x -= 1
		$AnimatedSprite.flip_h = true
	if(Input.is_key_pressed(KEY_S) && ! currentAreas.has(AREA.BOTTOM) && ! currentAreas.has(AREA.RIGHT)):
		velocity.y += 1
	if(Input.is_key_pressed(KEY_D) && ! currentAreas.has(AREA.RIGHT)):
		velocity.x += 1
		$AnimatedSprite.flip_h = false
	if velocity.length() > 0:
        velocity = velocity.normalized() * SPEED
	
	position += velocity * delta

func _on_FishPlayer_area_shape_entered(area_id, area, area_shape, self_shape):
	currentAreas.append(area_shape)

func _on_FishPlayer_area_shape_exited(area_id, area, area_shape, self_shape):
	currentAreas.erase(area_shape)
