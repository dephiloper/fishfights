extends Node

func _ready():
	disable_all_spots()

func _on_WormTimer_timeout():
	disable_all_spots()
	var childSize = $WormSpots.get_children().size()
	var randChildNumber = randi()%childSize
	var randSpot = $WormSpots.get_children()[randChildNumber]
	randSpot.disabled = false
	randSpot.visible = true
	
func disable_all_spots():
	for spot in $WormSpots.get_children():
		spot.disabled = true
		spot.visible = false

func _on_WormSpots_area_entered(area):
	if area == $FishPlayer:
		area.HEALTH += 3
		if (area.SPEED > 30):
			area.SPEED -= 30
		disable_all_spots()