extends Area2D

export (int) var SPEED = 200

var initialPosition
var humanPlayer
var fishArea

func _ready():
	initialPosition = position
	humanPlayer = $"/root/Main/HumanPlayer"
	fishArea = $"/root/Main/FishMovementArea"

func _process(delta):
	if (humanPlayer.currentState == humanPlayer.State.SINKING_SWIMMER):
		if(Input.is_key_pressed(KEY_LEFT)):
			position.x -= SPEED * delta
		if(Input.is_key_pressed(KEY_RIGHT)):
			position.x += SPEED * delta

func _on_FishingRodSwimmer_area_shape_entered(area_id, area, area_shape, self_shape):
	if (area == $"/root/Main/FishMovementArea"):
			if (humanPlayer.currentState == humanPlayer.State.THROW_SWIMMER):
				humanPlayer.currentState = humanPlayer.State.SINKING_SWIMMER
			else:
				humanPlayer.currentState = humanPlayer.State.MOVEMENT
				position = initialPosition
	if (area == $"/root/Main/FishPlayer"):
		get_tree().change_scene("res://Transition.tscn")
		Global.lives = $"/root/Main/FishPlayer".HEALTH